package example.main;

import org.junit.Test;
import static org.junit.Assert.*;

public class MapperTest
{

	@Test
	public void catMapperTest()
	{
		Car car = new Car("opel", 4);
		
		CarDto carDto = CarMapper.INSTANCE.carToCarDto(car);

		
		assertTrue(carDto.getMake().equals("opel"));
	}

}
